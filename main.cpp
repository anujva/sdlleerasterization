#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL.h>
#endif
#include <stdio.h>
#include "include/GzRender.h"

/* Structure for loaded sounds. */
typedef struct sound_s {
  Uint8 *samples;
  /* raw PCM sample data */
  Uint32 length;
  /* size of sound data in bytes */
} sound_t, *sound_p;

/* Structure for a currently playing sound. */
typedef struct playing_s {
  int active;
  /* 1 if this sound should be played */
  sound_p sound;
  /* sound data to play */
  Uint32 position;
  /* current position in the sound buffer */
}playing_t, *playing_p;

/* Array for all active sound effects. */
#define MAX_PLAYING_SOUNDS 10

playing_t playing[MAX_PLAYING_SOUNDS];

/* The higher this is, the louder each currently playing sound will be.
However, high values may cause distortion if too many sounds are
playing. Experiment with this. */
#define VOLUME_PER_SOUND SDL_MIX_MAXVOLUME / 2

/* This function is called by SDL whenever the sound card
needs more samples to play. It might be called from a
separate thread, so we should be careful what we touch. */
void AudioCallback(void *user_data, Uint8 * audio, int length){
  int i;
  /* Clear the audio buffer so we can mix samples into it. */
  memset(audio, 0, length);
  /* Mix in each sound. */
  for (i = 0; i < MAX_PLAYING_SOUNDS; i++) {
    if (playing[i].active) {
      Uint8 *sound_buf;
      Uint32 sound_len;
      /* Locate this sound’s current buffer position. */
      sound_buf = playing[i].sound->samples;
      sound_buf += playing[i].position;
      /* Determine the number of samples to mix. */
      if ((playing[i].position + length) > playing[i].sound->length) {
        sound_len = playing[i].sound->length - playing[i].position;
      } else {
        sound_len = length;
      };
      /* Mix this sound into the stream. */
      SDL_MixAudio(audio, sound_buf, sound_len, VOLUME_PER_SOUND);
      /* Update the sound buffer’s position. */
      playing[i].position += length;
      /* Have we reached the end of the sound? */
      if (playing[i].position >= playing[i].sound->length) {
        playing[i].active = 0;
        /* mark it inactive */
      };
    };
  };
}


int LoadAndConvertSound(char *filename, SDL_AudioSpec * spec, sound_p sound){
  SDL_AudioCVT cvt;
  /* audio format conversion structure */
  SDL_AudioSpec loaded;
  /* format of the loaded data */
  Uint8 *new_buf;
  /* Load the WAV file in its original sample format. */
  if (SDL_LoadWAV(filename,
    &loaded, &sound->samples, &sound->length) == NULL) {
    printf("Unable to load sound: %s\n", SDL_GetError());
    return 1;
  };
  /* Build a conversion structure for converting the samples.
  This structure contains the data SDL needs to quickly
  convert between sample formats. */
  if (SDL_BuildAudioCVT(&cvt, loaded.format, loaded.channels, loaded.freq, spec->format, spec->channels, spec->freq) < 0){
    printf("Unable to convert sound: %s\n", SDL_GetError());
    return 1;
  };
  /* Since converting PCM samples can result in more data
  (for instance, converting 8-bit mono to 16-bit stereo),
  we need to allocate a new buffer for the converted data.
  Fortunately SDL_BuildAudioCVT supplied the necessary
  information. */
  cvt.len = sound->length;
  new_buf = (Uint8 *) malloc(cvt.len * cvt.len_mult);
  if (new_buf == NULL) {
    printf("Memory allocation failed.\n");
    SDL_FreeWAV(sound->samples);
    return 1;
  };
  /* Copy the sound samples into the new buffer. */
  memcpy(new_buf, sound->samples, sound->length);

  /* Perform the conversion on the new buffer. */
  cvt.buf = new_buf;
  if (SDL_ConvertAudio(&cvt) < 0) {
    printf("Audio conversion error: %s\n", SDL_GetError());
    free(new_buf);
    SDL_FreeWAV(sound->samples);
    return 1;
  };
  /* Swap the converted data for the original. */
  SDL_FreeWAV(sound->samples);
  sound->samples = new_buf;
  sound->length = sound->length * cvt.len_mult;
  /* Success! */
  printf("’%s’ was loaded and converted successfully.\n", filename);
  return 0;
}

/* Removes all currently playing sounds. */
void ClearPlayingSounds(void){
  int i;
  for (i = 0; i < MAX_PLAYING_SOUNDS; i++) {
    playing[i].active = 0;
  };
}


/* Adds a sound to the list of currently playing sounds.
AudioCallback will start mixing this sound into the stream
the next time it is called (probably in a fraction of a second). */
int PlaySound(sound_p sound){
  int i;
  /* Find an empty slot for this sound. */
  for (i = 0; i < MAX_PLAYING_SOUNDS; i++){
    if (playing[i].active == 0)
    break;
  };
  /* Report failure if there were no free slots. */
  if (i == MAX_PLAYING_SOUNDS)
    return 1;
  /* The ’playing’ structures are accessed by the audio callback,
  so we should obtain a lock before we access them. */
  SDL_LockAudio();
  playing[i].active = 1;
  playing[i].sound = sound;
  playing[i].position = 0;
  SDL_UnlockAudio();
  return 0;
}



int main ( int argc, char** argv )
{
    // initialize SDL video
    if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);
    atexit(SDL_CloseAudio);

    // create a new window
    SDL_Surface* screen = SDL_SetVideoMode(256, 256, 32,
                                           SDL_HWSURFACE|SDL_DOUBLEBUF);
    if ( !screen )
    {
        printf("Unable to set 640x480 video: %s\n", SDL_GetError());
        return 1;
    }

    FILE *file;
    file = fopen("pot4.asc", "r");
    GzRender *render = new GzRender(&file);
    sound_t teapot;    
    SDL_AudioSpec desired, obtained;
    desired.freq = 44100;
    desired.format = AUDIO_S16;
    desired.samples = 4096;
    desired.channels = 2;
    desired.callback = AudioCallback;
    desired.userdata = NULL;
    if (SDL_OpenAudio(&desired, &obtained) < 0) {
      printf("Unable to open audio device: %s\n", SDL_GetError());
      return 1;
    };
    if (LoadAndConvertSound("teapot.wav", &obtained, &teapot) != 0) {
      printf("Unable to load sound.\n");
      return 1;
    };

    ClearPlayingSounds();
    SDL_PauseAudio(0);

    // program main loop
    bool done = false;
    
    while (!done)
    {
        // message processing loop
        PlaySound(&teapot);
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            // check for messages
            switch (event.type)
            {
                // exit if the window is closed
            case SDL_QUIT:
                done = true;
                break;

                // check for keypresses
            case SDL_KEYDOWN:
                {
                    // exit if ESCAPE is pressed
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        done = true;
                    else if(event.key.keysym.sym == SDLK_RIGHT){
                      render->draw(0);
                      SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

                      // draw bitmap
                      SDL_BlitSurface(render->frameBuffer, 0, screen, 0);

                      // DRAWING ENDS HERE

                      // finally, update the screen :)
                      SDL_Flip(screen);
                    }else if(event.key.keysym.sym == SDLK_LEFT){
                      render->draw(1);
                      SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

                      // draw bitmap
                      SDL_BlitSurface(render->frameBuffer, 0, screen, 0);

                      // DRAWING ENDS HERE

                      // finally, update the screen :)
                      SDL_Flip(screen);
                    }else if(event.key.keysym.sym == SDLK_UP){
                      render->draw(2);
                      SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

                      // draw bitmap
                      SDL_BlitSurface(render->frameBuffer, 0, screen, 0);

                      // DRAWING ENDS HERE

                      // finally, update the screen :)
                      SDL_Flip(screen);
                    }else if(event.key.keysym.sym == SDLK_DOWN){
                      render->draw(3);
                      SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

                      // draw bitmap
                      SDL_BlitSurface(render->frameBuffer, 0, screen, 0);

                      // DRAWING ENDS HERE

                      // finally, update the screen :)
                      SDL_Flip(screen);
                    }
                    break;
                }
            } // end switch
        } // end of message processing

        // DRAWING STARTS HERE

        // clear screen
        SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

        // draw bitmap
        SDL_BlitSurface(render->frameBuffer, 0, screen, 0);

        // DRAWING ENDS HERE

        // finally, update the screen :)
        SDL_Flip(screen);
    } // end main loop

    delete render;
    // free loaded bitmap
    SDL_PauseAudio(1);
    SDL_LockAudio();

    free(teapot.samples);

    SDL_UnlockAudio();

    // all is well ;)
    //SDL_Delay(9000);
    printf("Exited cleanly\n");
    return 0;
}
