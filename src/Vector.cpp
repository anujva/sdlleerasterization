#include "Vector.h"

Vector::Vector()
{
    //ctor
}

Vector::Vector(float a, float b, float c){
  x = a;
  y = b;
  z = c;
  homogenous = 1;
}

Vector::~Vector()
{
    //dtor
}

Vector& Vector::operator+(const Vector &rhs){
  Vector *result = new Vector(this->x + rhs.x, this->y + rhs.y, this->z + rhs.z);
  return *result;
}

Vector& Vector::operator=(const Vector &rhs){
  //check for self assignment
  if(this == &rhs){
    return *this;
  }else{
    this->x = rhs.x;
    this->y = rhs.y;
    this->z = rhs.z;
    this->homogenous = rhs.homogenous;
    return *this;
  }
}

void Vector::normalize(Vector &vec1){
  float mag = vec1.x*vec1.x + vec1.y*vec1.y + vec1.z*vec1.z;
  mag = sqrt(mag);
  scaleVec(vec1, mag);
}

void Vector::scaleVec(Vector &vec1, float factor){
  vec1.x = vec1.x/factor;
  vec1.y = vec1.y/factor;
  vec1.z = vec1.z/factor;
}

Vector& Vector::operator-(const Vector &vec2){
  this->x = this->x - vec2.x;
  this->y = this->y - vec2.y;
  this->z = this->z - vec2.z;

  return *this;
}

float Vector::dotProduct(Vector vec1, Vector vec2){
  float result = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
  return result;
}

Vector Vector::scale(Vector vec1, float factor){
  Vector result(vec1.x*factor, vec1.y*factor, vec1.z*factor);
  return result;
}

Vector Vector::crossProduct(Vector vec1, Vector vec2){
  Vector result;
  result.x = vec1.y * vec2.z - vec1.z * vec2.y;
  result.y = vec1.z * vec2.x - vec1.x * vec2.z;
  result.z = vec1.x * vec2.y - vec1.y * vec2.x;

  return result;
}

float Vector::operator[](int i){
  if(i==0){
    return x;
  }else if(i==1){
    return y;
  }else if(i==2){
    return z;
  }else if(i==3){
    return homogenous;
  }
}

void Vector::vectorCompMul(Vector *vec1, Vector *vec2, Vector *vec3){
  vec3->x = vec1->x * vec2->x;
  vec3->y = vec1->y * vec2->y;
  vec3->z = vec1->z * vec2->z;
}
