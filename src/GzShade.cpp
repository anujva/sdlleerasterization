#include "GzShade.h"

GzShade::GzShade(Vector **imageSpaceNormalList, Vector *vertexList){
  specpower = 32;
  this->imageSpaceNormalList = *imageSpaceNormalList; 
  //now that we have the imagespace normals.. we just need to compute the color at the normal.. and for that
  //we need a few things.. 

  GzLight light1 = 
    {
      {-0.7071, 0.7071, 0}, {0.5, 0.5, 0.9}
    };
  memcpy(&(this->light1), &light1, sizeof(GzLight));

  GzLight light2 = 
    {
      {0, -0.7071, -0.7071}, {0.9, 0.2, 0.3}
    };
  memcpy(&(this->light2), &light2, sizeof(GzLight));

  GzLight light3 = 
    {
      {0.7071, 0.0, -0.7071}, {0.2, 0.7, 0.3}
    };
  memcpy(&(this->light3), &light3, sizeof(GzLight));
  
  GzLight ambientLight = 
    {
      {0, 0, 0}, {0.3, 0.3, 0.3}
    };

  memcpy(&(this->ambientLight), &ambientLight, sizeof(GzLight));
  specularCoefficient = new Vector(0.3, 0.3, 0.3);
  ambientCoefficient = new Vector(0.1, 0.1, 0.1); 
  diffuseCoefficient = new Vector(0.7, 0.7, 0.7); 
  eyeDirection = new Vector(0, 0, -1);

  lights = new GzLight*[4];
  lights[0] = &light1;
  lights[1] = &light2;
  lights[2] = &light3;
  lights[3] = &ambientLight;

  Vector imageSpaceNormal;
  Vector* KdComponent, *KsComponent, *KaComponent;
  KdComponent = new Vector[3];
  KsComponent = new Vector[3];
  KaComponent = new Vector[3];
  colorResult = new Vector[3];
  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      imageSpaceNormal = this->imageSpaceNormalList[i];
      float N_L = Vector::dotProduct(imageSpaceNormal, lights[j]->direction);

      float N_E = Vector::dotProduct(imageSpaceNormal, *eyeDirection);
      
      if(N_E > 0 && N_L > 0){
        //don't do anything
      }else if(N_E < 0 && N_L < 0){
        imageSpaceNormal.x *= -1;
        imageSpaceNormal.y *= -1;
        imageSpaceNormal.z *= -1;
        N_E *= -1;
        N_L *= -1;
      }else{
        continue;
      }
      Vector reflectedRay;
      Vector N_L_2(2*N_L*imageSpaceNormal.x, 2*N_L*imageSpaceNormal.y, 2*N_L*imageSpaceNormal.z);
      reflectedRay.x = N_L_2.x - lights[j]->direction.x;
      reflectedRay.y = N_L_2.y - lights[j]->direction.y;
      reflectedRay.z = N_L_2.z - lights[j]->direction.z;
      float magRefRay = sqrt(reflectedRay.x*reflectedRay.x + reflectedRay.y*reflectedRay.y + reflectedRay.z*reflectedRay.z);
      Vector normRefRay(reflectedRay.x/magRefRay, reflectedRay.y/magRefRay, reflectedRay.z/magRefRay);

      float R_E = Vector::dotProduct(normRefRay, *eyeDirection);
      if(R_E < 0){
        R_E = 0;
      }

      float powerfactor = pow(R_E, specpower);
      Vector tempKsComponent(lights[j]->color.x*powerfactor, lights[j]->color.y*powerfactor, lights[j]->color.z*powerfactor);
      Vector tempKdComponent(lights[j]->color.x*N_L, lights[j]->color.y*N_L, lights[j]->color.z*N_L);
      KsComponent[i] = KsComponent[i]+tempKsComponent;
      KdComponent[i] = KdComponent[i]+tempKdComponent;
    }
    
    Vector::vectorCompMul(specularCoefficient, &KsComponent[i], &KsComponent[i]);
    Vector::vectorCompMul(diffuseCoefficient, &KdComponent[i], &KdComponent[i]);
    Vector::vectorCompMul(ambientCoefficient, &(ambientLight.color), &KaComponent[i]);
    colorResult[i] = KsComponent[i]+KdComponent[i];
    colorResult[i] = colorResult[i]+KaComponent[i];
  }
 
  Vector interpH1, interpH2, interpCrossProd;
  interpH1.x = -1*(vertexList[0].x - vertexList[1].x);
  interpH1.y = -1*(vertexList[0].y - vertexList[1].y);
  interpH2.x = -1*(vertexList[1].x - vertexList[2].x);
  interpH2.y = -1*(vertexList[1].y - vertexList[2].y);
  
  for(int i=0; i<3; i++){
    interpH1.z = (colorResult[vertexList[1].originalIndex][i] - colorResult[vertexList[0].originalIndex][i]);
    interpH2.z = (colorResult[vertexList[2].originalIndex][i] - colorResult[vertexList[1].originalIndex][i]);
    Vector crossProd = Vector::crossProduct(interpH1, interpH2);
    A[i] = crossProd.x;
    B[i] = crossProd.y;
    C[i] = crossProd.z;
    D[i] = -1*(A[i]*vertexList[1].x + B[i]*vertexList[1].y + C[i]*colorResult[vertexList[1].originalIndex][i]);
  }
}

Vector GzShade::returnColorAtPoint(int y, int x){
  Vector result;

  float colorR[3];
  for(int i=0; i<3; i++){
    colorR[i] = -1*(A[i]*x + B[i]*y + D[i])/C[i];
  }
  result.x = colorR[0];
  result.y = colorR[1];
  result.z = colorR[2];
  return result;
}
