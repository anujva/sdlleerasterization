#include "GzRender.h"

using namespace std;

GzRender::GzRender()
{
  //ctor
}

bool GzRender::shade2(Vector norm, Vector *color)
{
  float	light[3];
  float	coef;

  light[0] = 0.707f;
  light[1] = 0.5f;
  light[2] = 0.5f;

  coef = light[0]*norm.x + light[1]*norm.y + light[2]*norm.z;
  if (coef < 0) 	coef *= -1;

  if (coef > 1.0)	coef = 1.0;
  color->x = coef*0.95f;
  color->y = coef*0.65f;
  color->z = coef*0.88f;

  return true;
}

GzRender::GzRender(FILE **myfile){
  myModelFile = *myfile;
  Vector *vertexList = new Vector[3];
  Vector *normalList = new Vector[3];
  Vector *screenSpaceVertexList = new Vector[3];
  Vector *imageSpaceNormalList = new Vector[3];
  Vector *uvList = new Vector[2];
  char dummy[256];
  matlevel = 0;
  matlevelNorm = 0;
  int rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif
  frameBuffer = SDL_CreateRGBSurface(SDL_HWSURFACE, 256, 256, 32, rmask, gmask, bmask, amask);
  SDL_LockSurface(frameBuffer);
  pixels = (unsigned int*)(frameBuffer->pixels);
  z_depth = new float*[256];
  display_res_x = 256;
  display_res_y = 256;
  for(int i=0; i<256; i++){
      z_depth[i] = new float[256];
  }
  for(int i=0; i<256; i++){
      for(int j=0; j<256; j++){
          z_depth[i][j] = INT_MAX;
      }
  }
  
  //Initialize the camera here.. 
  //set up the parameters for camera here.. 

  Vector cameraPosition(13.2, -8.7, -14.8);
  Vector look(0.8, 0.7, 4.5);
  Vector world(-0.2,1,0);
  float fov = 53.7;
  initializeCameraAndNormal(cameraPosition, look, world, fov);

  Vector color;
  while(fscanf(myModelFile, "%s", dummy) == 1){ 	/* read in tri word */
          fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
              &(vertexList[0].x), &(vertexList[0].y),
              &(vertexList[0].z),
              &(normalList[0].x), &(normalList[0].y),
              &(normalList[0].z),
              &(uvList[0].x), &(uvList[0].y) );
          fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
              &(vertexList[1].x), &(vertexList[1].y),
              &(vertexList[1].z),
              &(normalList[1].x), &(normalList[1].y),
              &(normalList[1].z),
              &(uvList[1].x), &(uvList[1].y) );
          fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
              &(vertexList[2].x), &(vertexList[2].y),
              &(vertexList[2].z),
              &(normalList[2].x), &(normalList[2].y),
              &(normalList[2].z),
              &(uvList[2].x), &(uvList[2].y) );
          shade2(normalList[0], &color);
          //we have to transform the vertexList to get the screen space coordinates
          //That is the only way this will work
          vertexList[0].originalIndex = 0;
          vertexList[1].originalIndex = 1;
          vertexList[2].originalIndex = 2;
          TransformToScreenSpace(vertexList, &screenSpaceVertexList);
          TransformToImageSpace(normalList, &imageSpaceNormalList);
          //all normals are normalized as well so there is nothing to worry about
          LeeRasterizer(screenSpaceVertexList, imageSpaceNormalList, color);
  }
  SDL_UnlockSurface(frameBuffer);
  delete[] vertexList;
  delete[] normalList;
  delete[] screenSpaceVertexList;
  delete[] uvList;
}

void GzRender::BuildXsp(){
  //lets initialize the Xpi matrix
  float d = 1/tan((camera->FOV*PI/180)/2);
  GzMatrix Xsp_ph = {
    {
      display_res_x/2, 0, 0, display_res_x/2 
    },
    {
      0, -1*display_res_y/2, 0, display_res_y/2
    },
    {
      0, 0, INT_MAX/d, 0
    },
    {
      0, 0, 0, 1
    }
  };
  
  memcpy(Xsp, Xsp_ph, sizeof(Xsp_ph));
}

void GzRender::BuildXpi(){
  float d = 1/(tan((camera->FOV*PI/180)/2));
  GzMatrix Xpi_ph = {
    {
      1, 0, 0, 0
    },
    {
      0, 1, 0, 0
    },
    {
      0, 0, 1, 0
    },
    {
      0, 0, 1/d, 1
    }
  };

  memcpy(Xpi, Xpi_ph, sizeof(Xpi_ph));
}

void GzRender::BuildXiw(){
  //Need to find the camera X Y Z axes.. This can be calculated using the camera values
  //of position lookat and worldup.
  Vector cl(camera->lookat.x - camera->position.x, camera->lookat.y - camera->position.y, camera->lookat.z - camera->position.z);
  Vector::normalize(cl);
  Vector cameraZ = cl;
  float dotProductResult = Vector::dotProduct(camera->worldup, cameraZ);
  //need to scale cameraZ by this result and then subtract
  Vector scaledZ = Vector::scale(cameraZ, dotProductResult);
  Vector cameraUp = camera->worldup - scaledZ;
  Vector::normalize(cameraUp);
  Vector cameraY = cameraUp;

  //X = Y x Z
  Vector cameraX = Vector::crossProduct(cameraY, cameraZ);
  Vector::normalize(cameraX);

  GzMatrix Xiw_ph = {
    {
      cameraX.x, cameraX.y, cameraX.z, -1*Vector::dotProduct(cameraX, camera->position)
    },
    {
      cameraY.x, cameraY.y, cameraY.z, -1*Vector::dotProduct(cameraY, camera->position)
    },
    {
      cameraZ.x, cameraZ.y, cameraZ.z, -1*Vector::dotProduct(cameraZ, camera->position)
    },
    {
      0, 0, 0, 1
    }
  };

  memcpy(Xiw, Xiw_ph, sizeof(Xiw_ph));
}

void GzRender::matrixMultiply(GzMatrix m1, GzMatrix m2, GzMatrix result){
  for(int i=0; i<4; i++){
    for(int j=0; j<4; j++){
      float sum = 0;
      for(int k=0; k<4; k++){
        sum += m1[i][k] * m2[k][j];
      }
      result[i][j] = sum;
    }
  }
}

void GzRender::pushMatrix(GzMatrix mat){
  if(matlevel < MAXLEVELS){
    if(matlevel == 0)
      memcpy(Ximage[matlevel++], mat, sizeof(GzMatrix));
    else{
      GzMatrix resultant;
      matrixMultiply(Ximage[matlevel-1], mat, resultant);
      memcpy(Ximage[matlevel++], resultant, sizeof(GzMatrix));
    }
  }else{
    matlevel = 6;
  }
}

void GzRender::pushMatrixNorm(GzMatrix mat){
  //preprocess the mat variable so that the translate values are all one and the scale
  //should also not affect.. so take care of that as well
  for(int i=0; i<3; i++){
    mat[i][3] = 0;
  }

  float normalizefactor = sqrt(mat[0][0]*mat[0][0]+mat[0][1]*mat[0][1]+mat[0][2]*mat[0][2]);
  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      mat[i][j] /= normalizefactor;
    }
  }

  //we have not ensured that the matrices are unitary rotation matrices.. so no sweat
  if(matlevelNorm < MAXLEVELS){
    if(matlevelNorm == 0)
      memcpy(Xnorm[matlevelNorm++], mat, sizeof(GzMatrix));
    else{
      GzMatrix resultant;
      matrixMultiply(Xnorm[matlevelNorm-1], mat, resultant);
      memcpy(Xnorm[matlevelNorm++], resultant, sizeof(GzMatrix));
    }
  }else{
    matlevelNorm = 3;
  }
}

void GzRender::popMatrix(){
  if(matlevel > 0){
    //remove
    matlevel--;
  }else{
    //do nothing
  }
}

void GzRender::popMatrixNorm(){
  if(matlevelNorm > 0){
    //remove
    matlevelNorm--;
  }else{
    //do nothing
  }
}

void GzRender::initializeCameraAndNormal(Vector position, Vector look, Vector worldup, float fov){
  camera = new GzCamera(position, look, fov);
  camera->worldup = worldup;
  BuildXiw();
  BuildXpi();
  BuildXsp();  
  pushMatrix(Xsp);
  pushMatrix(Xpi);
  pushMatrix(Xiw);
  pushMatrixNorm(Xiw);

  GzMatrix scale = {
    {3.25,   0.0,    0.0,    0.0}, 
    {0.0,    3.25,   0.0,    -3.25}, 
    {0.0,    0.0,    3.25,   3.5}, 
    {0.0,    0.0,    0.0,    1.0} 
  };
  GzMatrix rotateX = {
    {1.0,    0.0,    0.0,    0.0}, 
    {0.0,    .7071,  .7071,  0.0}, 
    {0.0,    -.7071, .7071,  0.0}, 
    {0.0,    0.0,    0.0,    1.0} 
  };
  GzMatrix rotateY = {
    {.866,   0.0,    -0.5,   0.0}, 
    {0.0,    1.0,    0.0,    0.0}, 
    {0.5,    0.0,    .866,   0.0}, 
    {0.0,    0.0,    0.0,    1.0} 
  };

  pushMatrix(scale);
  pushMatrix(rotateY);
  pushMatrix(rotateX);
  pushMatrixNorm(scale);
  pushMatrixNorm(rotateY);
  pushMatrixNorm(rotateX);
}

void GzRender::TransformToScreenSpace(Vector *vertexList, Vector **screenSpaceVertexList){
  //We need to define the transforms and for that we need to build the matrices
  Vector *ssVertex = new Vector[3];
  for(int i=0; i<3; i++){

    for(int j=0; j<4; j++){
      float sum = 0;
      for(int k=0; k<4; k++){
        if(k!=3)
          sum += Ximage[matlevel-1][j][k] * vertexList[i][k];//.x;
        else
          sum += Ximage[matlevel-1][j][k];
        //sum += Ximage[matlevel-1][j][1] * vertexList[i].y;
        //sum += Ximage[matlevel-1][j][2] * vertexList[i].z;
        //sum += Ximage[matlevel-1][j][3] * 1;
      }
      if(j==0)
        ssVertex[i].x = sum;
      else if(j == 1){
        ssVertex[i].y = sum;
      }else if(j == 2){
        ssVertex[i].z = sum;
      }else if(j == 3){
        ssVertex[i].homogenous = sum;
      }
    }

    ssVertex[i].x = ssVertex[i].x/ssVertex[i].homogenous;
    ssVertex[i].y = ssVertex[i].y/ssVertex[i].homogenous;
    ssVertex[i].z = ssVertex[i].z/ssVertex[i].homogenous;
    ssVertex[i].homogenous = ssVertex[i].homogenous/ssVertex[i].homogenous;
    ssVertex[i].originalIndex = vertexList[i].originalIndex;
  }

  *screenSpaceVertexList = ssVertex;
}

void GzRender::TransformToImageSpace(Vector *vertexList, Vector **imageSpaceNormalList){
  //We need to define the transforms and for that we need to build the matrices
  Vector *ssVertex = new Vector[3];
  for(int i=0; i<3; i++){

    for(int j=0; j<4; j++){
      float sum = 0;
      for(int k=0; k<4; k++){
        if(k!=3)
          sum += Xnorm[matlevelNorm-1][j][k] * vertexList[i][k];//.x;
        else
          sum += Xnorm[matlevelNorm-1][j][k];
        //sum += Ximage[matlevel-1][j][1] * vertexList[i].y;
        //sum += Ximage[matlevel-1][j][2] * vertexList[i].z;
        //sum += Ximage[matlevel-1][j][3] * 1;
      }
      if(j==0)
        ssVertex[i].x = sum;
      else if(j == 1){
        ssVertex[i].y = sum;
      }else if(j == 2){
        ssVertex[i].z = sum;
      }else if(j == 3){
        ssVertex[i].homogenous = sum;
      }
    }

    ssVertex[i].x = ssVertex[i].x/ssVertex[i].homogenous;
    ssVertex[i].y = ssVertex[i].y/ssVertex[i].homogenous;
    ssVertex[i].z = ssVertex[i].z/ssVertex[i].homogenous;
    ssVertex[i].homogenous = ssVertex[i].homogenous/ssVertex[i].homogenous;
  }

  *imageSpaceNormalList = ssVertex;
}

int GzRender::getBoundingBox(Vector *vertexList, float *xmin, float *xmax, float *ymin, float *ymax){
  Vector *vec1 = &vertexList[0];
  Vector *vec2 = &vertexList[1];
  Vector *vec3 = &vertexList[2];

  //find the minimum and maximum of the vertex
  if(vec1->x > vec2->x){
      //we have to test for vec1->x and vec3->x
      if(vec1->x > vec3->x){
          //we are sure that vec1->x is max.. but we need to check again for min
          *xmax = vec1->x;
          if(vec3->x > vec2->x){
              *xmin = vec2->x;
          }else{
              *xmin = vec3->x;
          }
      }else{
          //vec1 was greater than vec2 but was less than vec3
          //clearly vec3 is biggest here and vec2 is least
          *xmax = vec3->x;
          *xmin = vec2->x;
      }
  }else{
      //vec1 was less than vec2
      if(vec2->x > vec3->x){
          *xmax = vec2->x;
          if(vec1->x > vec3->x){
              *xmin = vec3->x;
          }else{
              *xmin = vec1->x;
          }
      }else{
          *xmax = vec3->x;
          *xmin = vec1->x;
      }
  }

  //now for the y value
  if(vec1->y > vec2->y){
      //we have to test for vec1->x and vec3->x
      if(vec1->y > vec3->y){
          //we are sure that vec1->x is max.. but we need to check again for min
          *ymax = vec1->y;
          if(vec3->y > vec2->y){
              *ymin = vec2->y;
          }else{
              *ymin = vec3->y;
          }
      }else{
          //vec1 was greater than vec2 but was less than vec3
          //clearly vec3 is biggest here and vec2 is least
          *ymax = vec3->y;
          *ymin = vec2->y;
      }
  }else{
      //vec1 was less than vec2
      if(vec2->y > vec3->y){
          *ymax = vec2->y;
          if(vec1->y > vec3->y){
              *ymin = vec3->y;
          }else{
              *ymin = vec1->y;
          }
      }else{
          *ymax = vec3->y;
          *ymin = vec1->y;
      }
  }

  return 0;
}

int compareX(const void *v1, const void *v2){
  Vector *vec1 = (Vector*)v1;
  Vector *vec2 = (Vector*)v2;

  float diff = vec1->x - vec2->x;

  if(diff>0){
      return 1;
  }else if(diff<0){
      return -1;
  }else{
      return 0;
  }
}

int comareYX(const void *v1, const void *v2){
  Vector *vec1 = (Vector*)v1;
  Vector *vec2 = (Vector*)v2;

  float diff = vec1->y - vec2->y;
  if(diff<0){
      return -1;
  }else if(diff>0){
      return 1;
  }else{
      return compareX(v1, v2);
  }
}

int GzRender::sortVertices(Vector *vertexList){
  qsort(vertexList, 3, sizeof(Vector), comareYX);
  return 0;
}

bool GzRender::getABC(float *A, float *B, float *C, Edge edge){
  if(!A||!B||!C){
      return false;
  }

  *A = edge.end2.y - edge.end1.y;
  *B = -1*(edge.end2.x - edge.end1.x);
  *C = ( (edge.end2.x - edge.end1.x) * edge.end1.y ) - ( (edge.end2.y - edge.end1.y) * edge.end1.x );

  return true;
}

int GzRender::LeeRasterizer(Vector *screenSpaceVertexList, Vector *imageSpaceNormalList, Vector color){
  //I already have the vertices in screen space
  //Begin by getting the bounding box
  float xmin, xmax, ymin, ymax;
  getBoundingBox(screenSpaceVertexList, &xmin, &xmax, &ymin, &ymax);
  xmin = std::max((int)xmin, 0);
  xmax = std::min(int(xmax), display_res_x-1);
  ymin = std::max((int)ymin, 0);
  ymax = std::min((int)ymax, display_res_y-1);

  //sort the vertexList first Y then X
  Edge edges[3];
  sortVertices(screenSpaceVertexList);
  if(screenSpaceVertexList[0].y == screenSpaceVertexList[1].y){
      Edge::copyEdge(&edges[0].end1, &screenSpaceVertexList[1]);
      Edge::copyEdge(&edges[0].end2, &screenSpaceVertexList[0]);
      edges[0].edgeColor = COLORED_EDGE;
      Edge::copyEdge(&edges[1].end1, &screenSpaceVertexList[0]);
      Edge::copyEdge(&edges[1].end2, &screenSpaceVertexList[2]);
      edges[1].edgeColor = COLORED_EDGE;
      Edge::copyEdge(&edges[2].end1, &screenSpaceVertexList[2]);
      Edge::copyEdge(&edges[2].end2, &screenSpaceVertexList[1]);
      edges[2].edgeColor = NOCOLOR_EDGE;
  }else if(screenSpaceVertexList[1].y == screenSpaceVertexList[2].y){
      Edge::copyEdge(&edges[0].end1, &screenSpaceVertexList[0]);
      Edge::copyEdge(&edges[0].end2, &screenSpaceVertexList[1]);
      edges[0].edgeColor = COLORED_EDGE;
      Edge::copyEdge(&edges[1].end1, &screenSpaceVertexList[1]);
      Edge::copyEdge(&edges[1].end2, &screenSpaceVertexList[2]);
      edges[1].edgeColor = NOCOLOR_EDGE;
      Edge::copyEdge(&edges[2].end1, &screenSpaceVertexList[2]);
      Edge::copyEdge(&edges[2].end2, &screenSpaceVertexList[0]);
      edges[2].edgeColor = NOCOLOR_EDGE;
  }else{
      float slope = (screenSpaceVertexList[2].y - screenSpaceVertexList[0].y)/(screenSpaceVertexList[2].x - screenSpaceVertexList[0].x);
      float midpointX = ((screenSpaceVertexList[1].y- screenSpaceVertexList[0].y)/slope) + screenSpaceVertexList[0].x;

      if( midpointX < screenSpaceVertexList[1].x ){
          Edge::copyEdge(&edges[0].end1, &screenSpaceVertexList[0]);
          Edge::copyEdge(&edges[0].end2, &screenSpaceVertexList[2]);
          edges[0].edgeColor = COLORED_EDGE;

          Edge::copyEdge(&edges[1].end1, &screenSpaceVertexList[2]);
          Edge::copyEdge(&edges[1].end2, &screenSpaceVertexList[1]);
          edges[1].edgeColor = NOCOLOR_EDGE;

          Edge::copyEdge(&edges[2].end1, &screenSpaceVertexList[1]);
          Edge::copyEdge(&edges[2].end2, &screenSpaceVertexList[0]);
          edges[2].edgeColor = NOCOLOR_EDGE;
      }else if( midpointX > screenSpaceVertexList[1].x ){
          Edge::copyEdge(&edges[0].end1, &screenSpaceVertexList[0]);
          Edge::copyEdge(&edges[0].end2, &screenSpaceVertexList[1]);
          edges[0].edgeColor = COLORED_EDGE;
          Edge::copyEdge(&edges[1].end1, &screenSpaceVertexList[1]);
          Edge::copyEdge(&edges[1].end2, &screenSpaceVertexList[2]);
          edges[1].edgeColor = COLORED_EDGE;
          Edge::copyEdge(&edges[2].end1, &screenSpaceVertexList[2]);
          Edge::copyEdge(&edges[2].end2, &screenSpaceVertexList[0]);
          edges[2].edgeColor = NOCOLOR_EDGE;
      }else{
          //error
          return false;
      }
  }

  //we have the edges now.. but we need to get the equations of the lines these edges represent.. that means we need to get
  //A B and C in the general form of the equation of the line Ax+By+C = 0;

  float A1, B1, C1, A2, B2, C2, A3, B3, C3;
  getABC(&A1, &B1, &C1, edges[0]);
  getABC(&A2, &B2, &C2, edges[1]);
  getABC(&A3, &B3, &C3, edges[2]);

  //for z buffering we need to calculate the equation of the plane
  Vector result;
  Edge::crossProduct(edges[0], edges[1], &result);
  float pA = result.x;
  float pB = result.y;
  float pC = result.z;
  float pD = -(pA * screenSpaceVertexList[0].x + pB * screenSpaceVertexList[0].y + pC * screenSpaceVertexList[0].z);


  //we have the equation of the edges.. now we can begin the rasterization process..
  
  GzShade shadingObject(&imageSpaceNormalList, screenSpaceVertexList);
  for(int i=ymin; i<=ymax; i++){
      for(int j=xmin; j<=xmax; j++){
          bool isShaded = false;

          float firstResult = A1*j + B1*i + C1;
          if(firstResult == 0){
              if(edges[0].edgeColor == COLORED_EDGE){
                  isShaded = true;
              }else{
                  continue;
              }
          }else if(firstResult<0){
              continue;
          }

          if(!isShaded){
              float secondResult = A2*j + B2*i + C2;
              if(secondResult == 0){
                  if(edges[0].edgeColor == COLORED_EDGE){
                      isShaded = true;
                  }else{
                      continue;
                  }
              }else if(secondResult<0){
                  continue;
              }
          }

          if(!isShaded){
              float thirdResult = A3*j + B3*i + C3;
              if(thirdResult == 0){
                  if(edges[0].edgeColor == COLORED_EDGE){
                      isShaded = true;
                  }else{
                      continue;
                  }
              }else if(thirdResult<0){
                  continue;
              }
          }

          float Zind = -(pA * j + pB * i  + pD) / pC;

          if( Zind < 0 )
                  continue;

          //Get the current z value in the buffer;
           //printf("Value of i and j: %d %d\n", i, j);
          if(Zind < z_depth[i][j]){
              //put the pixel color in the frame buffer
              //printf("Value of Color returned: %d \n", colorReturn(color));
              z_depth[i][j] = Zind;
              //instead of just returning the color we need to use the imageSpaceNormalList

              color = shadingObject.returnColorAtPoint(i, j);
              pixels[i*display_res_x+j] = colorReturn(color);
          }
      }
  }

  return 0;
}

unsigned int GzRender::colorReturn(Vector color){
  int red = color.x*256;
  int green = color.y*256;
  int blue = color.z*256;

  if(red > 255){
    red =255;
  }else if(red < 0){
    red = 0;
  }
        
  if(green > 255){
    green =255;
  }else if(green < 0){
    green = 0;
  }
            
  if(blue > 255){
    blue =255;
  }else if(blue < 0){
    blue = 0;
  }

  return ((red)|(green<<8)|(blue<<16));
}

GzRender::~GzRender(){
  //delete vertexList, normalList, screenSpaceVertexList, uvList, dummy 
  delete camera;
  for(int i=0; i<256; i++){
    delete[] z_depth[i];
  }
  delete[] z_depth;
  SDL_FreeSurface(frameBuffer);
}


void GzRender::draw(int direction){
  if(direction == 0){
    GzMatrix rotateY = {
      {.866,   0.0,    -0.5,   0.0}, 
      {0.0,    1.0,    0.0,    0.0}, 
      {0.5,    0.0,    .866,   0.0}, 
      {0.0,    0.0,    0.0,    1.0} 
    };
    pushMatrix(rotateY);
  }else if(direction == 1){
    GzMatrix rotateY = {
      {.866,   0.0,    0.5,   0.0}, 
      {0.0,    1.0,    0.0,    0.0}, 
      {-0.5,    0.0,    .866,   0.0}, 
      {0.0,    0.0,    0.0,    1.0}
    };
    pushMatrix(rotateY);
  }else if(direction == 2){
    GzMatrix rotateX = {
      {1.0,    0.0,    0.0,    0.0}, 
      {0.0,    .7071,  .7071,  0.0}, 
      {0.0,    -.7071, .7071,  0.0}, 
      {0.0,    0.0,    0.0,    1.0} 
    };
    pushMatrix(rotateX);
  }else if(direction == 3){
    GzMatrix rotateX = {
      {1.0,    0.0,    0.0,    0.0}, 
      {0.0,    .7071,  -.7071,  0.0}, 
      {0.0,    .7071, .7071,  0.0}, 
      {0.0,    0.0,    0.0,    1.0} 
    };
    pushMatrix(rotateX);
  }

  SDL_FillRect(frameBuffer, NULL, 0);
  SDL_LockSurface(frameBuffer);
  Vector *vertexList = new Vector[3];
  Vector *normalList = new Vector[3];
  Vector *screenSpaceVertexList = new Vector[3];
  Vector *imageSpaceNormalList = new Vector[3];
  Vector *uvList = new Vector[2];
  char dummy[256];
  for(int i=0; i<256; i++){
      for(int j=0; j<256; j++){
          z_depth[i][j] = INT_MAX;
      }
  }
  Vector color;
  rewind(myModelFile);
  while(fscanf(myModelFile, "%s", dummy) == 1){ 	/* read in tri word */
      fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
          &(vertexList[0].x), &(vertexList[0].y),
          &(vertexList[0].z),
          &(normalList[0].x), &(normalList[0].y),
          &(normalList[0].z),
          &(uvList[0].x), &(uvList[0].y) );
      fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
          &(vertexList[1].x), &(vertexList[1].y),
          &(vertexList[1].z),
          &(normalList[1].x), &(normalList[1].y),
          &(normalList[1].z),
          &(uvList[1].x), &(uvList[1].y) );
      fscanf(myModelFile, "%f %f %f %f %f %f %f %f",
          &(vertexList[2].x), &(vertexList[2].y),
          &(vertexList[2].z),
          &(normalList[2].x), &(normalList[2].y),
          &(normalList[2].z),
          &(uvList[2].x), &(uvList[2].y) );
      shade2(normalList[0], &color);
      //we have to transform the vertexList to get the screen space coordinates
      //That is the only way this will work
      vertexList[0].originalIndex = 0;
      vertexList[1].originalIndex = 1;
      vertexList[2].originalIndex = 2;

      TransformToScreenSpace(vertexList, &screenSpaceVertexList);
      TransformToImageSpace(normalList, &imageSpaceNormalList);
      LeeRasterizer(screenSpaceVertexList, imageSpaceNormalList, color);
  }

  SDL_UnlockSurface(frameBuffer);
}
