#ifndef GZCAMERA_H
#define GZCAMERA_H

#include "Vector.h"

class GzCamera
{
    public:
        GzCamera();
        GzCamera(Vector pos, Vector look, float field);
        virtual ~GzCamera();
        Vector position;
        Vector lookat;
        Vector worldup;
        float FOV; // Field of View
};

#endif // GZCAMERA_H
