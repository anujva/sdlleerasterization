#ifndef GZRENDER_H
#define GZRENDER_H

#include <SDL/SDL.h>
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <algorithm>
#include "GzCamera.h"
#include "GzShade.h"
#include "Edge.h"

#define MAXLEVELS 100

/* define general display pixel-type */

#define COLORED_EDGE 0
#define NOCOLOR_EDGE 1

#define INT_MAX 2147483647
#define PI 3.14159265

class GzRender
{
    private:
        //what are the things that we are going to need for this rendering system
        //1. a framebuffer.. can we not just use the SDL Surface? Yes, we can
        //2. we need to have model files reading into to get the tris
        //3. we need to have a camera.. but a camera should be its own class.
        class Color{
            public:
                int red;
                int green;
                int blue;
                int alpha;

                Color(){
                    red = 255;
                    green = 255;
                    blue = 255;
                    alpha = 255;
                }
        };

        int display_res_x;
        int display_res_y;
        float **z_depth;
        FILE *myModelFile;
        GzCamera *camera;
        GzMatrix Ximage[MAXLEVELS];
        GzMatrix Xnorm[MAXLEVELS];
        int matlevel;
        int matlevelNorm;
        GzMatrix Xsp;
        GzMatrix Xpi;
        GzMatrix Xiw;
        int numLights;
        Color flatcolor;
        unsigned int *pixels;

    public:
        GzRender();
        ~GzRender();
        GzRender(FILE **myfile);
        SDL_Surface *frameBuffer;
        int getBoundingBox(Vector* vertexList, float *xmin, float *xmax, float *ymin, float *ymax);
        int sortVertices(Vector *vertexList);
        bool getABC(float *A, float *B, float *C, Edge edge);
        bool shade2(Vector, Vector*);
        unsigned int colorReturn(Vector color);
        void TransformToScreenSpace(Vector *modelSpaceVertexList, Vector **screenSpaceVertexList);
        void TransformToImageSpace(Vector *modelSpaceNormalList, Vector **imageSpaceNormalList);
        void BuildXsp();
        void BuildXpi();
        //Before doing this we need to initialize the camera
        //Make sure of this.. 
        void BuildXiw();
        void popMatrix();
        void popMatrixNorm();
        void matrixMultiply(GzMatrix, GzMatrix, GzMatrix);
        void pushMatrix(GzMatrix mat);
        void pushMatrixNorm(GzMatrix mat);
        void initializeCameraAndNormal(Vector v, Vector l, Vector w, float f);
        void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
        int LeeRasterizer(Vector* modelSpaceVertex, Vector* imageSpaceNormalList, Vector color);
        void draw(int);
};

#endif // GZRENDER_H
