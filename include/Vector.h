#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>

typedef float	GzMatrix[4][4];


class Vector
{
    public:
        Vector();
        Vector(float a, float b, float c);
        virtual ~Vector();

        //will have 4 dimensions
        float x, y, z;
        float homogenous;
        int originalIndex;
        Vector& operator=(const Vector &rhs);
        static void normalize(Vector &);
        static void scaleVec(Vector &, float);
        Vector& operator-(const Vector &vec1);
        Vector& operator+(const Vector &vec1);
        static float dotProduct(Vector vec1, Vector vec2);
        static Vector scale(Vector, float);
        static Vector crossProduct(Vector, Vector);
        float operator[](int i);
        static void vectorCompMul(Vector*, Vector*, Vector*); 
};

#endif // VECTOR_H
