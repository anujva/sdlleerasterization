#ifndef EDGE_H
#define EDGE_H

#include "Vector.h"

class Edge{
    public:
        Vector end1;
        Vector end2;
        int edgeColor;

        Edge(){
            end1.x = 0;
            end1.y = 0;
            end1.z = 0;
            end1.homogenous = 1;

            end2.x = 0;
            end2.y = 0;
            end2.z = 0;
            end2.homogenous = 1;
        }

        static void copyEdge(Vector *vertex1, Vector *vertex2){
            vertex1->x = vertex2->x;
            vertex1->y = vertex2->y;
            vertex2->z = vertex2->z;
        }

        static int crossProduct(Edge edge1, Edge edge2, Vector *result){
            Vector vector1, vector2;
            vector1.x = edge1.end2.x - edge1.end1.x;
            vector1.y = edge1.end2.y - edge1.end1.y;
            vector1.z = edge1.end2.z - edge1.end1.z;
            vector2.x = edge2.end2.x - edge2.end1.x;
            vector2.y = edge2.end2.y - edge2.end1.y;
            vector2.z = edge2.end2.z - edge2.end1.z;
            result->x = vector1.y * vector2.z - vector1.z * vector2.y;
            result->y = -( vector1.x * vector2.z - vector1.z * vector2.x );
            result->z = vector1.x * vector2.y - vector1.y * vector2.x;

            return 0;
        }

        ~Edge();
};

#endif // EDGE_H
