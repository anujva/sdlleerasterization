INSTPATH = ./bin
SRCPATH = ./src
INCPATH = ./include
OBJPATH = ./obj
CC = g++
CFLAGS = -g -Wall -I$(INCPATH)
SDLFLAGS = `sdl-config --cflags --libs`
COND1 = `stat 3DRendering 2>/dev/null | grep Modify`
COND2 = `stat $(INSTPATH) 2>/dev/null | grep Modify`

all:	getobj app install putobj

app: main.o Vector.o Edge.o GzCamera.o GzRender.o GzShade.o
					$(CC) $(CFLAGS) -o 3DRendering main.o Edge.o GzCamera.o GzRender.o GzShade.o Vector.o $(SDLFLAGS)	
					
main.o:	main.cpp $(INCPATH)/GzRender.h
					$(CC) $(CFLAGS) -c main.cpp $(SDLFLAGS)	
					
GzCamera.o: $(SRCPATH)/GzCamera.cpp $(INCPATH)/GzCamera.h $(INCPATH)/Vector.h
					$(CC) $(CFLAGS) -c $(SRCPATH)/GzCamera.cpp
					
Vector.o: $(SRCPATH)/Vector.cpp $(INCPATH)/Vector.h
					$(CC) $(CFLAGS) -c $(SRCPATH)/Vector.cpp
					
Edge.o: $(SRCPATH)/Edge.cpp $(INCPATH)/Edge.h $(INCPATH)/Vector.h
					$(CC) $(CFLAGS) -c $(SRCPATH)/Edge.cpp
					
GzRender.o: $(SRCPATH)/GzRender.cpp $(INCPATH)/Edge.h $(INCPATH)/Vector.h $(INCPATH)/GzCamera.h $(INCPATH)/GzShade.h
					$(CC) $(CFLAGS) -c $(SRCPATH)/GzRender.cpp
					
GzShade.o:  $(SRCPATH)/GzShade.cpp $(INCPATH)/GzShade.h $(INCPATH)/Vector.h
					$(CC) $(CFLAGS) -c $(SRCPATH)/GzShade.cpp

getobj:
					-mv $(OBJPATH)/*.o . 
					
putobj:
					-mv *.o $(OBJPATH)
					
install:
					@if [ "$(COND1)" != "$(COND2)" ];\
					then \
					    cp -p ./3DRendering $(INSTPATH) 2>/dev/null;\
					    chmod 755 $(INSTPATH);\
					    echo "Installed in " $(INSTPATH);\
					fi

cleanall:
					-rm -f 3DRendering
					-rm -f $(OBJPATH)/*.o
					-rm -f $(INSTPATH)


